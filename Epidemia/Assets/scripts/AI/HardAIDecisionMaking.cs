﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HardAIDecisionMaking : MonoBehaviour, IAIDecisions
{
    [SerializeField] int criticalDamageThreshold;

    private BoardManager boardManager;
    private Player playerReference;

    private List<int> playerScores = new List<int>();

    private List<BaseCard> cardsInHand = new List<BaseCard>();

    private bool criticalDamage;

    private void Awake()
    {
        boardManager = GameObject.Find("Board Manager").GetComponent<BoardManager>();
        playerReference = GetComponent<Player>();
        playerReference.IsAI = true;
    }



    public void PlayTurn()
    {
        criticalDamage = false;

        if (cardsInHand.Count == 0) // con esto agregas posicioes en la lista como en serialized
        {
            for (int i = 0; i < 5; i++)
            {
                cardsInHand.Add(null);
            }
        }

        for (int i = 0; i < 5; i++)
        {
            cardsInHand[i] = playerReference.CardsInHandGO[i].GetComponent<BaseCard>();
        }

        if (playerReference.HasLowDefenses)
        {
            for (int i = 0; i < 5; i++)
            {
                if (cardsInHand[i].CardID == 12) // altas defensas wachin
                {
                    for (int j = 0; j < playerReference.PlayerChips.Count; j++)
                    {
                        if (playerReference.PlayerChips[j].ChipType == 5)
                        {
                            boardManager.Players[boardManager.ActualPlayer].HandOffset = new Vector3(cardsInHand[i].gameObject.transform.localPosition.x, 0, 0);
                            cardsInHand[i].UseDefenses(playerReference.PlayerChips[j], playerReference);
                            cardsInHand[i].Discard(playerReference.Deck);
                            boardManager.TurnFinished();
                            return;
                        }

                    }

                }
            }
        }

        FillListOfPlayerScores(playerScores);

        playerScores[boardManager.ActualPlayer] *= 2; // en que momento le decimos que deje de multiplicar

        Debug.LogError(playerScores[boardManager.ActualPlayer] + " " + criticalDamageThreshold);

        if (playerScores[boardManager.ActualPlayer] >= criticalDamageThreshold)
        {
            if (HealState() == true)
            {
                boardManager.TurnFinished();
                return;
            }
        }

        for (int i = 0; i < playerScores.Count; i++)
        {
            int randomLocal = Random.Range(0, playerScores.Count);

            if (playerScores[randomLocal] >= criticalDamageThreshold)
            {
                if (AttackState(randomLocal) == true)
                {
                    boardManager.TurnFinished();
                    return;
                }
            }
            randomLocal++;
            if (randomLocal > playerScores.Count)
            {
                randomLocal = 0;
            }
        }

        int randomPlayer = Random.Range(0, boardManager.Players.Count);

        for (int i = 0; i < boardManager.Players.Count; i++)
        {
            if (randomPlayer == boardManager.Players.Count)
            {
                randomPlayer = 0;
            }

            if (randomPlayer != boardManager.ActualPlayer)
            {
                if (AttackState(randomPlayer) == true)
                {
                    boardManager.TurnFinished();
                    return;
                }

            }
            randomPlayer++;
        }
        int aux = Random.Range(0, 5);
        boardManager.Players[boardManager.ActualPlayer].HandOffset = new Vector3(cardsInHand[aux].gameObject.transform.localPosition.x, 0, 0);
        cardsInHand[aux].Discard(playerReference.Deck);
        boardManager.TurnFinished();
    }

    private void FillListOfPlayerScores(List<int> playerScores)
    {
        if (playerScores.Count == 0)//agregas posiciones en lista. creas los espacios como en serialized
        {
            for (int i = 0; i < boardManager.AmmountOfPlayers; i++)
            {
                playerScores.Add(0);
            }
        }

        for (int i = 0; i < boardManager.AmmountOfPlayers; i++)
        {

            playerScores[i] = CalculateScore(boardManager.Players[i]);
            Debug.LogWarning(playerScores[i] + "score de los players");
        }

    }

    private int CalculateScore(Player player)
    {
        int score = 0;

        if (player.HasLowDefenses)
        {
            score += 40;
        }

        for (int i = 0; i < 5; i++)
        {
            if (!player.PlayerChips[i].IsHealthy)
            {
                score += 20;
            }
        }
        int aux = Random.Range(0, 2);
        if (player.IsAI == false)
        {
            if (aux == 1)
            {
                score += 20;
            }
        }
        else
        {
            if (aux == 1)
            {
                score -= 20;
            }
        }
        if (score >= criticalDamageThreshold)
        {
            criticalDamage = true;
        }
        return score;
    }

    private bool HealState()
    {
        int start = Random.Range(0, 5); // para que no arranque siempre en cerebro

        bool success = false;

        for (int i = 0; i < playerReference.PlayerChips.Count; i++) //buscar chips, pregntar en mis chips cuales estan infectadas
        {
            if (start > 4)
            {
                start = 0;
            }

            if (!playerReference.PlayerChips[start].IsHealthy) //juega carta de curacion 
            {
                Debug.LogError(cardsInHand.Count + "cantidad de cartas en mano");

                for (int j = 0; j < cardsInHand.Count; j++)
                {
                    Debug.LogError(cardsInHand[j].CardType + "tipo de carta adentro del for");

                    if (cardsInHand[j].CardType == 0)
                    {
                        BaseHealCard heal = cardsInHand[j].GetComponent<BaseHealCard>();

                        if (heal.allowed == playerReference.PlayerChips[start].ChipType)
                        {
                            Debug.LogError("pude curar");
                            boardManager.Players[boardManager.ActualPlayer].HandOffset = new Vector3(cardsInHand[j].gameObject.transform.localPosition.x, 0, 0);
                            cardsInHand[j].Use(playerReference.PlayerChips[start]);
                            cardsInHand[j].Discard(playerReference.Deck);
                            success = true;
                            return success;
                        }
                    }
                    else if (cardsInHand[j].CardType == 2) // juega carta de transplante si no tiene de cura
                    {
                        BaseMisc misc = cardsInHand[j].GetComponent<BaseMisc>();

                        if (misc.MiscType == 2)
                        {
                            int aux = Random.Range(0, boardManager.Players.Count);

                            for (int k = 0; k < boardManager.Players.Count; k++)
                            {
                                if (aux > boardManager.Players.Count - 1)
                                {
                                    aux = 0;
                                }

                                if (boardManager.Players[aux].PlayerChips[start].IsHealthy)
                                {
                                    boardManager.Players[aux].PlayerChips[start].IsHealthy = false;
                                    playerReference.PlayerChips[start].IsHealthy = true;

                                    boardManager.Players[aux].PlayerChips[start].Animator.SetBool("hasBeenDamaged", true);
                                    boardManager.Players[aux].PlayerChips[start].Animator.SetBool("hasBeenHealed", false);

                                    playerReference.PlayerChips[start].Animator.SetBool("hasBeenDamaged", false);
                                    playerReference.PlayerChips[start].Animator.SetBool("hasBeenHealed", true);

                                    boardManager.Players[boardManager.ActualPlayer].HandOffset = new Vector3(cardsInHand[j].gameObject.transform.localPosition.x, 0, 0);
                                    cardsInHand[j].Discard(playerReference.Deck);
                                    success = true;
                                    return success;
                                }
                                aux++;
                            }
                        }
                    }
                }
            }
            start++;
        }
        return success;
    }


    private bool AttackState(int playerIndex)
    {
        bool success = false;

        if (playerIndex == boardManager.ActualPlayer)
        {
            return success;
        }

        for (int i = 0; i < boardManager.Players[playerIndex].PlayerChips.Count; i++)
        {
            if (boardManager.Players[playerIndex].PlayerChips[i].IsHealthy)
            {
                int counter = 0;

                for (int j = 0; j < cardsInHand.Count; j++)
                {
                    if (cardsInHand[j].CardType == 1) // si tengo carta de matar
                    {
                        BaseDisease disease = cardsInHand[j].GetComponent<BaseDisease>();

                        if ((disease.allowed1 == boardManager.Players[playerIndex].PlayerChips[i].ChipType || disease.allowed2 == boardManager.Players[playerIndex].PlayerChips[i].ChipType))
                        {
                            boardManager.Players[boardManager.ActualPlayer].HandOffset = new Vector3(cardsInHand[j].gameObject.transform.localPosition.x, 0, 0);
                            cardsInHand[j].Use(boardManager.Players[playerIndex].PlayerChips[i]);
                            cardsInHand[j].Discard(playerReference.Deck);
                            success = true;
                            return success;
                        }

                    }
                    else if (cardsInHand[j].CardType == 2) // si tengo carta de misc
                    {
                        BaseMisc misc = cardsInHand[j].GetComponent<BaseMisc>();

                        int start = Random.Range(0, 5);

                        if (misc.MiscType == 0)
                        {
                            if (boardManager.Players[playerIndex].PlayerChips[i].IsHealthy == true && boardManager.Players[playerIndex].PlayerChips[i].ChipType == 5 && boardManager.Players[playerIndex].PlayerID != boardManager.ActualPlayer)
                            {
                                boardManager.Players[boardManager.ActualPlayer].HandOffset = new Vector3(cardsInHand[j].gameObject.transform.localPosition.x, 0, 0);
                                cardsInHand[j].UseDefenses(boardManager.Players[playerIndex].PlayerChips[i], boardManager.Players[playerIndex]);
                                cardsInHand[j].Discard(playerReference.Deck);
                                success = true;
                                return success;
                            }

                        }

                        if (misc.MiscType == 3) //carta de epidemia
                        {
                            for (int k = 0; k < playerReference.PlayerChips.Count; k++)
                            {
                                if (start > 4)
                                {
                                    start = 0;
                                }

                                if (!playerReference.PlayerChips[start].IsHealthy && boardManager.Players[playerIndex].PlayerChips[start].IsHealthy) // uso carta de epidemia
                                {
                                    for (int o = 0; o < boardManager.Players.Count; o++)
                                    {
                                        boardManager.Players[o].PlayerChips[start].IsHealthy = false;
                                        boardManager.Players[o].PlayerChips[start].Animator.SetBool("hasBeenDamaged", true);
                                        boardManager.Players[o].PlayerChips[start].Animator.SetBool("hasBeenHealed", false);
                                    }
                                    boardManager.Players[boardManager.ActualPlayer].HandOffset = new Vector3(cardsInHand[j].gameObject.transform.localPosition.x, 0, 0);
                                    cardsInHand[j].Discard(playerReference.Deck);
                                    success = true;
                                    return success;
                                }

                            }

                        }
                        if (misc.MiscType == 2) // juega transplante
                        {
                            if (!playerReference.PlayerChips[start].IsHealthy)
                            {
                                boardManager.Players[boardManager.ActualPlayer].HandOffset = new Vector3(cardsInHand[j].gameObject.transform.localPosition.x, 0, 0);
                                boardManager.Players[playerIndex].PlayerChips[start].IsHealthy = false;
                                playerReference.PlayerChips[start].IsHealthy = true;

                                playerReference.PlayerChips[start].Animator.SetBool("hasBeenHealed", true);
                                playerReference.PlayerChips[start].Animator.SetBool("hasBeenDamaged", false);

                                boardManager.Players[playerIndex].PlayerChips[start].Animator.SetBool("hasBeenDamaged", true);
                                boardManager.Players[playerIndex].PlayerChips[start].Animator.SetBool("hasBeenHealed", false);

                                cardsInHand[j].Discard(playerReference.Deck);
                                success = true;
                                return success;
                            }
                        }
                    }
                    else //no se encontro ninguna carta para usar
                    {
                        counter++;
                        if (counter == 5)
                        {
                            success = false;
                            return success;
                        }
                    }
                }
            }
        }
        return success;
    }
}

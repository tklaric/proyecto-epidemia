﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiverChip : BaseChip
{
    public override void ChangeState()
    {
        base.ChangeState();
        Animator.SetBool("hasBeenDamaged", !IsHealthy);
        Animator.SetBool("hasBeenHealed", IsHealthy);
    }
    protected override void Awake()
    {
        base.Awake();
        ChipType = 3;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TetanusDisease : BaseDisease
{

    //afecta liver y brain
    protected override void Awake()
    {
        base.Awake();
        allowed1 = 0; //brain
        allowed2 = 3; //liver
        CardID = 0;
    }

    private void Update()
    {

        if (IsSelected == true)
        {
            Debug.Log("tetanus seleccionada");
            // funcionalidad para poder elegir cual de los 2 chips atacar.
            // if(ficha seleccionada corresponde a alguna de las 2){ use(), descartar carta }
        }
    }

    public override void Use(BaseChip chip)
    {
        Debug.Log("tetanus usado");

        chip.ChangeState();

        if (chip.Player.HasLowDefenses == true)
        {
            if (chip.Player.PlayerChips[allowed1].IsHealthyChip())
            {
                chip.Player.PlayerChips[allowed1].ChangeState();
            }

            if (chip.Player.PlayerChips[allowed2].IsHealthyChip())
            {
                chip.Player.PlayerChips[allowed2].ChangeState();
            }
        }
    }
}

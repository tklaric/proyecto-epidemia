﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sequence : BehaviourNode
{
    int lastProcessingIndex;
    List<BehaviourNode> children = new List<BehaviourNode>();

    public override bool CanExecute(AIController ai)
    {
        for (int i = 0; i < children.Count; i++)
            if (!children[i].CanExecute(ai))
                return false;

        return true;
    }


    void Awake()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            var childTransform = transform.GetChild(i);
            var childNode = childTransform.GetComponent<BehaviourNode>();

            if (childNode != null)
                children.Add(childNode);
            else
                Debug.LogError("Missing Node in Sequence", childTransform.gameObject);
        }
    }

    protected override BehaviourResult ExecuteInternal(AIController ai)
    {

        for (int i = 0; i < lastProcessingIndex; i++)
        {
            var child = children[i];
            if (!child.CanExecute(ai))
            {
                //Como se que voy a cambiar la ejecucion a otro nodo desde uno que estaba
                //processing, le digo a ese que estaba en processing que finalize
                children[lastProcessingIndex].End(ai);

                //Si uno de los de la izquierda del que es processing se puede ejecutar
                //le digo al Selector que a continuacion arranque ejecutando ese
                lastProcessingIndex = i;
                break;
            }
        }



        for (int i = lastProcessingIndex; i < children.Count; i++)
        {
            var child = children[i];
            var result = child.Execute(ai);

            if (result == BehaviourResult.Processing)
            {
                lastProcessingIndex = i;
                return BehaviourResult.Processing;
            }

            if (result == BehaviourResult.Failure)
            {
                lastProcessingIndex = 0;
                return BehaviourResult.Failure;
            }
        }

        lastProcessingIndex = 0;
        return BehaviourResult.Success;
    }

    public override void End(AIController ai)
    {
        if (LastResult == BehaviourResult.Processing)
            children[lastProcessingIndex].End(ai);

        lastProcessingIndex = 0;
    }
}

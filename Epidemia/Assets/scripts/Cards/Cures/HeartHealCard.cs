﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartHealCard : BaseHealCard
{
    protected override void Awake()
    {
        base.Awake();
        allowed = 1;
        CardID = 7;
    }

    public override void Use(BaseChip chip)
    {
        chip.ChangeState();
    }
}

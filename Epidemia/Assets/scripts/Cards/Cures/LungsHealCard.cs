﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LungsHealCard : BaseHealCard
{
    protected override void Awake()
    {
        base.Awake();
        allowed = 2;
        CardID = 10;
    }
    private void Update()
    {
        if(IsSelected == true){
            Debug.Log("lungs seleccionada");
        }
    }
    public override void Use(BaseChip chip)
    {
        chip.ChangeState();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using TMPro;

public class PauseOptionsMenu : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI musicVolumeText;

    [SerializeField] TextMeshProUGUI gameSoundsVolumeText;

    [SerializeField] AudioMixer musicVolumeMixer;

    [SerializeField] AudioMixer gameSoundsMixer;

    private float musicVolume = 100;
    private float gameSoundsVolume = 100;

    private void Start()
    {
        musicVolumeMixer.SetFloat("MusicVolume", musicVolume - 80); // seteo el volumen al mango
        gameSoundsMixer.SetFloat("GameSoundsVolume", gameSoundsVolume - 80); // seteo el volumen al mango

        musicVolumeText.text = musicVolume.ToString(); // hago de cuenta que 20db es igual a 100 en texto
        gameSoundsVolumeText.text = gameSoundsVolume.ToString();

    }

    public void IncreaseVolume()
    {
        musicVolume += 10;

        if (musicVolume >= 100)
        {
            musicVolume = 100;
        }

        musicVolumeMixer.SetFloat("MusicVolume", musicVolume - 80);

        musicVolumeText.text = musicVolume.ToString();
    }

    public void DeacreaseVolume()
    {
        musicVolume -= 10;

        if (musicVolume <= 0)
        {
            musicVolume = 0;
        }
        musicVolumeMixer.SetFloat("MusicVolume", musicVolume - 80);

        musicVolumeText.text = musicVolume.ToString();
    }

    public void IncreaseGameSoundsVolume()
    {
        gameSoundsVolume += 10;

        if (gameSoundsVolume >= 100)
        {
            gameSoundsVolume = 100;
        }

        gameSoundsMixer.SetFloat("GameSoundsVolume", gameSoundsVolume - 80);

        gameSoundsVolumeText.text = gameSoundsVolume.ToString();
    }

    public void DecreaseGameSoundsVolume()
    {
        gameSoundsVolume -= 10;

        if (gameSoundsVolume <= 0)
        {
            gameSoundsVolume = 0;
        }

        gameSoundsMixer.SetFloat("GameSoundsVolume", gameSoundsVolume - 80);

        gameSoundsVolumeText.text = gameSoundsVolume.ToString();
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AutoLocalization;
using TMPro;
using SaveSystem;

public class LanguageMenu: MonoBehaviour
{
    public List<TextMeshProUGUI> textProToTranslate = new List<TextMeshProUGUI>();
    [SerializeField] List<string> textInEnglish = new List<string>();


    private bool hasBeenTranslated;

    public bool HasBeenTranslated { get => hasBeenTranslated; set => hasBeenTranslated = value; }


    private void Awake()
    {
        for (int i = 0; i < textInEnglish.Count; i++)
        {
            textInEnglish[i] = textProToTranslate[i].text;
        }        
    }
   
    public void TranslateToSpanish()
    {
        for (int i = 0; i < textProToTranslate.Count; i++)
        {
            textProToTranslate[i].text = LanguageManager.instance.GetMeaning(textProToTranslate[i].text, Languages.Spanish).ToUpper();
            textProToTranslate[0].text = textProToTranslate[0].text.ToLower();
            textProToTranslate[3].text = "MODO INCURABLE";
            textProToTranslate[11].text = "COMENZAR";
            textProToTranslate[13].text = "JUGAR";
            textProToTranslate[15].text = "DESBLOQUEABLES*";
            textProToTranslate[17].text = "créditos".ToUpper();
            textProToTranslate[20].text = "* Estas funciones estarán disponibles pronto. Estamos trabajando en ellas";
        }
        HasBeenTranslated = true;
    }

    public void TranslateToEnglish()
    {
        for (int i = 0; i < textProToTranslate.Count; i++)
        {
            textProToTranslate[i].text = textInEnglish[i];
        }
        hasBeenTranslated = false;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Text.RegularExpressions;

public class PlayerMenu : MonoBehaviour
{

    private string defaultName;

    private int counterAux;
    private int initialCounter;
    private int dificultyCounter;
    private int maxCharacters = 10;

    private bool hasBeenDisabled;

    private bool easyDifficulty;
    private bool normalDifficulty;
    private bool hardDifficulty;
    private bool cpuIsActive = false;
    private bool isAI;

    [SerializeField] int counter = 0;
    [SerializeField] AvatarManager avatarManager;
    [SerializeField] List<PlayerMenu> players = new List<PlayerMenu>();
    [SerializeField] List<Image> avatars = new List<Image>();
    [SerializeField] List<Image> cpuAvatars = new List<Image>();
    [SerializeField] LanguageMenu languageMenuRef;
    [SerializeField] TMP_InputField nameInputField;
    [SerializeField] TextMeshProUGUI placeHolderText;
    [SerializeField] GameObject playerSelectorText;
    [SerializeField] GameObject cpuSelectorText;
    [SerializeField] GameObject avatarText;
    [SerializeField] GameObject easyText;
    [SerializeField] GameObject normalText;
    [SerializeField] GameObject hardText;


    public bool CpuIsActive { get => cpuIsActive; set => cpuIsActive = value; }
    public bool IsAI { get => isAI; set => isAI = value; }
    public bool EasyDifficulty { get => easyDifficulty; set => easyDifficulty = value; }
    public bool NormalDifficulty { get => normalDifficulty; set => normalDifficulty = value; }
    public bool HardDifficulty { get => hardDifficulty; set => hardDifficulty = value; }
    public TMP_InputField NameInputField { get => nameInputField; set => nameInputField = value; }
    public List<Image> Avatars { get => avatars; set => avatars = value; }
    public int Counter { get => counter; set => counter = value; }
    public GameObject EasyText { get => easyText; set => easyText = value; }
    public List<Image> CpuAvatars { get => cpuAvatars; set => cpuAvatars = value; }
    public int CounterAux { get => counterAux; set => counterAux = value; }

    private void Awake()
    {
        NameInputField.characterLimit = maxCharacters;
        defaultName = placeHolderText.text;
    }

    private void Start()
    {
        //Avatars[0].enabled = false;
        //Avatars[counter].enabled = true;
        ResetAvatar();

       

    }

   

    private void Update()
    {
        
        NameInputField.text = Regex.Replace(NameInputField.text, @"[^a-zA-Z0-9 ]", ""); // evita usar todos esos caracteres especiales
        if (cpuIsActive == false)
        {
            EasyText.SetActive(false);
            easyDifficulty = false;

            normalText.SetActive(false);
            normalDifficulty = false;

            hardText.SetActive(false);
            hardDifficulty = false;

            if (!languageMenuRef.HasBeenTranslated)
            {
                placeHolderText.text = defaultName;
            }
            
            NameInputField.interactable = true;

        }
        //Debug.Log(counter + gameObject.name);
    }

    public void ChangeText()
    {
        if (CpuIsActive == false)
        {
            playerSelectorText.SetActive(false);
            cpuSelectorText.SetActive(true);
            CpuIsActive = true;
        }      
        else
        {
            playerSelectorText.SetActive(true);
            cpuSelectorText.SetActive(false);
            CpuIsActive = false;
        }
    }

    public void ActivateDificultyBox()
    {
        counter = 6;

        if (CpuIsActive == true)
        {
            EasyText.SetActive(true);
            easyDifficulty = true;
            placeHolderText.text = "EASY";
            NameInputField.interactable = false;
            avatarText.SetActive(false);
            //ResetAvatar();
            CpuAvatars[0].enabled = true;

        }
        else
        {
            EasyText.SetActive(false);
            easyDifficulty = false;

            normalText.SetActive(false);
            normalDifficulty = false;

            hardText.SetActive(false);
            hardDifficulty = false;

            dificultyCounter = 0;
            avatarText.SetActive(true);

            for (int i = 0; i < CpuAvatars.Count; i++)
            {
                CpuAvatars[i].enabled = false;
            }
            
            ChangeAvatarLeft();          
        }
    }

    public void DificultySelector()
    {

        if (dificultyCounter == 0 && cpuIsActive)
        {
            EasyText.SetActive(true);
            easyDifficulty = true;
            CpuAvatars[0].enabled = true;
            placeHolderText.text = "EASY";
            NameInputField.interactable = false;

            normalText.SetActive(false);
            normalDifficulty = false;
            CpuAvatars[1].enabled = false;

            hardText.SetActive(false);
            hardDifficulty = false;
            CpuAvatars[2].enabled = false;
        }
        else if (dificultyCounter == 1 && cpuIsActive)
        {
            EasyText.SetActive(false);
            easyDifficulty = false;
            CpuAvatars[0].enabled = false;


            normalText.SetActive(true);
            normalDifficulty = true;
            CpuAvatars[1].enabled = true;
            placeHolderText.text = "NORMAL";
            NameInputField.interactable = false;

            hardText.SetActive(false);
            hardDifficulty = false;
            CpuAvatars[2].enabled = false;

        }
        else if (dificultyCounter == 2 && cpuIsActive) 
        {
            EasyText.SetActive(false);
            easyDifficulty = false;
            CpuAvatars[0].enabled = false;


            normalText.SetActive(false);
            normalDifficulty = false;
            CpuAvatars[1].enabled = false;


            hardText.SetActive(true);
            hardDifficulty = true;
            CpuAvatars[2].enabled = true;
            placeHolderText.text = "HARD";
            NameInputField.interactable = false;

        }
    }

    public void ChangeAvatarRight()
    {


        if (cpuIsActive)
        {
            Avatars[Counter].enabled = true;
            Avatars[Counter - 1].enabled = false;
        }
        else
        {
            initialCounter = counter;

            counter++;

            if (Counter > 5)
            {
                Counter = 5;
            }

            for (int i = 0; i < 6; i++)
            {
                Avatars[i].enabled = false;
            }

            //bool auxBool = false;
           

            for (int i = counter; i < 6; i++)
            {
                bool match = false;

                for (int j = 0; j < players.Count; j++)
                {
                    Debug.Log("J ES: " + j + " PLAYER COUNTER " + players[j].Counter);
                    if (i == players[j].counter)
                    {
                        match = true;
                        Debug.Log("no va pasar nada");
                        break;
                    }

                }
                if (i == 5 && match == true)
                {
                    avatars[initialCounter].enabled = true;
                    Debug.Log("no puedo cambiar avatr a la derecha");
                    counter = initialCounter;  
                    break;
                }
                if (match == false)
                {
                    Debug.Log("entre al for con la letra K");
                    for (int k = 0; k < 6; k++)
                    {
                        Avatars[k].enabled = false;
                    }

                    Avatars[i].enabled = true;
                    counter = i;
                    break;
                }
                
                Debug.Log("initial counter: "+initialCounter);
            }            
        }
    }

    public void ChangeAvatarLeft()
    {
        if (cpuIsActive)
        {
            Avatars[Counter].enabled = true;
            Avatars[Counter + 1].enabled = false;
        }
        else
        {
            initialCounter = counter;

            counter--;

            if (Counter < 0)
            {
                Counter = 0;
            }

            for (int i = 0; i < 6; i++)
            {
                Avatars[i].enabled = false;
            }

            for (int i = counter; i > -1; i--)
            {
                bool match = false;

                for (int j = 0; j < players.Count; j++)
                {
                    Debug.Log("J ES: " + j + " PLAYER COUNTER " + players[j].Counter);
                    if (i == players[j].counter)
                    {
                        match = true;
                        Debug.Log("no va pasar nada");
                        break;
                    }

                }
                if (i == 0 && match == true)
                {
                    avatars[initialCounter].enabled = true;
                    Debug.Log("no puedo cambiar avatr a la derecha");
                    counter = initialCounter;
                    break;
                }
                if (match == false)
                {
                    Debug.Log("entre al for con la letra K");
                    for (int k = 0; k < 6; k++)
                    {
                        Avatars[k].enabled = false;
                    }

                    Avatars[i].enabled = true;
                    counter = i;
                    break;
                }

                Debug.Log("initial counter: " + initialCounter);
            }
        }

    }

    public void ResetAvatar()
    {
        avatarManager.AvailableAvatars[counter] = true;

        counter = 0;
        for (int i = 0; i < avatars.Count; i++)
        {
            avatars[i].enabled = false;
        }
        for (int i = 0; i < 6; i++)
        {
            if (avatarManager.AvailableAvatars[i])
            {
                avatars[i].enabled = true;
                avatarManager.AvailableAvatars[i] = false;
                counter = i;
                break;
            }
        }
        //avatars[0].enabled = true;
        //counter = 0;
    }

    public void OnEnable()
    { 
        ChangeAvatarLeft();   
        ResetAvatar();
    }

    public void OnDisable()
    {
        CounterAux = counter;

        counter = 6;

        if (!cpuIsActive)
        {
            avatarManager.AvailableAvatars[counter] = true;
        }
       
    }

    public void IncreaseDificultyCounter()
    {
        dificultyCounter++;
       // Counter++;

        if (dificultyCounter > 2)
        {
            dificultyCounter = 0;
        }

        //if (Counter > 5) 
        //{
        //    Counter = 5;
        //}
    }

    public void DecreaseDificultyCounter()
    {
        dificultyCounter--;
        //Counter--;

        if (dificultyCounter < 0)
        {
            dificultyCounter = 2;
        }

        //if (Counter<0)
        //{
        //    Counter = 0;
        //}

    }
}

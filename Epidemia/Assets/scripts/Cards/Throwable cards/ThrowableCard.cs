﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowableCard : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] float torquePower;
    [SerializeField] float widthRandomness;
    [SerializeField] float timeUntilDeactivation;

    private Rigidbody rigidbody;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
    }
    private void OnEnable()
    {
        float width = Random.Range(-widthRandomness, widthRandomness);
        
        Invoke("Deactivate", timeUntilDeactivation);

        //rigidbody.AddRelativeForce(Vector3.forward * speed + Vector3.right * width);
        rigidbody.AddRelativeForce(100,speed,-100);
        rigidbody.AddTorque(0,Random.Range(-torquePower,torquePower),0);
    }
    private void Deactivate()
    {
        gameObject.SetActive(false);
    }
    //private void FixedUpdate()
    //{
    //    rigidbody.AddTorque(0, torquePower, 0);
    //}

}

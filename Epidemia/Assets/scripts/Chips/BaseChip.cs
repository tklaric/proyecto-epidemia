﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseChip : MonoBehaviour
{
    [SerializeField] Player player;
    [SerializeField] bool isHealthy;
    [SerializeField] Light chipLight;
    private int chipType; //0 brain, 1 heart, 2 lungs, 3 liver, 4 kidneys
    private bool isSelected;
    private Animator animator;
    private BoxCollider collider;
    

    public bool IsHealthy { get => isHealthy; set => isHealthy = value; }
    public int ChipType { get => chipType; set => chipType = value; }
    public BoxCollider Collider { get => collider; set => collider = value; }
    public Player Player { get => player; set => player = value; }
    public bool IsSelected { get => isSelected; set => isSelected = value; }
    public Animator Animator { get => animator; set => animator = value; }
    public Light ChipLight { get => chipLight; set => chipLight = value; }

    public bool IsHealthyChip()
    {
        return isHealthy;
    }
    protected virtual void Awake()
    {
        Collider = GetComponent<BoxCollider>();
        Animator = GetComponent<Animator>();
        IsHealthy = true;
    }
    private void Start()
    {
        //player = GetComponentInParent<Player>();

    }
    public virtual void ChangeState()
    {

        IsHealthy = !IsHealthy;
        Animator.SetBool("hasBeenDamaged", !IsHealthy);
        Animator.SetBool("hasBeenHealed", IsHealthy);
        Debug.Log(isHealthy);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Clase06_BlurPostProcess : MonoBehaviour
{
    public Material mat;

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        var tempTexture = RenderTexture.GetTemporary(source.width, source.height);//GUARDAMOS LA TEXTURA QUE VAMOS A MODIFICAR EN LA PRIMERA PASADA

        Graphics.Blit(source, tempTexture, mat, 0);//APLICAMOS EL PRIMER PASS
        Graphics.Blit(tempTexture, destination, mat, 1);//APLICAMOS EL SEGUNDO PASS

        RenderTexture.ReleaseTemporary(tempTexture);//DESCARTAMOS LA TEXTURA TEMPORAL
    }
}

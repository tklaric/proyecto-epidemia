﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Player : MonoBehaviour
{

    [SerializeField] Deck deck;
    [SerializeField] List<BaseChip> playerChips = new List<BaseChip>();
    [SerializeField] TextMeshProUGUI playerNameUi;
    [SerializeField] Image playerAvatar;
    [SerializeField] bool hasLowDefenses = false;
    


    private ThrowCardSpawner tcspawnerReference;

    private int playerID;
    private int totalInfectedChips;

    public bool isDead;
    private bool isAI;

    private Vector3 handOffset;
    private List<GameObject> cardsInHand = new List<GameObject>();
    
    public Vector3 HandOffset { get => handOffset; set => handOffset = value; }
    public int PlayerID { get => playerID; set => playerID = value; }
    public List<GameObject> CardsInHandGO { get => cardsInHand; set => cardsInHand = value; }
    public bool HasLowDefenses { get => hasLowDefenses; set => hasLowDefenses = value; }
    public List<BaseChip> PlayerChips { get => playerChips; set => playerChips = value; }
    public int TotalInfectedChips { get => totalInfectedChips; set => totalInfectedChips = value; }
    public bool IsDead { get => isDead; set => isDead = value; }
    public bool IsAI { get => isAI; set => isAI = value; }
    public Deck Deck { get => deck; set => deck = value; }
    public TextMeshProUGUI PlayerNameUi { get => playerNameUi; set => playerNameUi = value; }
    public Image PlayerAvatar { get => playerAvatar; set => playerAvatar = value; }

    private void Start()
    {
        Deck = GameObject.Find("Deck").GetComponent<Deck>();
        tcspawnerReference = GameObject.Find("TCSpawner").GetComponent<ThrowCardSpawner>();

    }
   
    private void SpawnThrowableCard()
    {
       
    }

    public void DrawCard()
    {
        tcspawnerReference.transform.LookAt(gameObject.transform.position);
        tcspawnerReference.SpawnCard();

        if (Deck.CardsByIdInDeck.Count <= 0)
        {
            Deck.CardsByIdInDeck = Deck.CardsByIdDiscarded;
            Deck.CardsByIdDiscarded = new List<int>();
            Deck.Shuffle();
        }
        GameObject cardGo = Instantiate(Deck.CardTypesList[Deck.CardsByIdInDeck[0]],gameObject.transform);

        if (!isAI)
        {
            cardGo.GetComponent<BaseCard>().ActivateRendererInSetTime(0.5f);
        }
        
        //cardGo.GetComponent<Renderer>().enabled = true; //activarlo medio segundo despues
        cardGo.transform.localPosition += handOffset;
        cardGo.transform.localPosition += new Vector3(0,-0.443f,0.798f);
        cardGo.transform.localScale = new Vector3(5, 5, 5);
        CardsInHandGO.Add(cardGo);     

        Deck.CardsByIdInGame.Add(Deck.CardsByIdInDeck[0]);
        Deck.CardsByIdInDeck.RemoveAt(0);
    }
    

    public void CheckDeath()
    {
        int aux = 0;
        for (int i = 0; i < playerChips.Count-1; i++)
        {
            if (playerChips[i].IsHealthy == false)
            {
                aux++;
                if (aux == 5)
                {
                    isDead = true;                  
                    for (int j = 0; j < playerChips.Count-1; j++)
                    {
                        playerChips[j].gameObject.layer = 0;
                        PlayerChips[5].gameObject.layer = 0;
                    }
                }
            }
        }
    }
}

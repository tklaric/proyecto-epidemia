﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseMisc : BaseCard
{
    //protected Player playerReference;

    private int miscType;

    public int MiscType { get => miscType; set => miscType = value; }

    protected virtual void Awake()
    {
        //playerReference = GetComponentInParent<Player>();
        CardType = 2;
    }
}

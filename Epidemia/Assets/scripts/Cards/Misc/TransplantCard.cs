﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransplantCard : BaseMisc
{
    protected override void Awake()
    {
        base.Awake();
        MiscType = 2;
        CardID = 5;
    }

    public override void Use(BaseChip chip) // no usamos este Use ya q afecta el bajas/Altas defensas 
    {
        Debug.Log("translplante hacido");
        chip.ChangeState();
    }
}

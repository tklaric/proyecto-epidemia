﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BillboardEffect : MonoBehaviour
{

    private Transform cameraTransform;
    private Quaternion originalRotation;

    public Transform CameraTransform { get => cameraTransform; set => cameraTransform = value; }

    void Start()
    {
        
        originalRotation = transform.rotation;    
    }

  

    void Update()
    {


        //cameraTransform = Camera.main.transform;
        transform.LookAt(Camera.main.transform);
        //transform.rotation = CameraTransform.rotation * originalRotation;

    }
}

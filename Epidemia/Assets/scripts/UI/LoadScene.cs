﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    [SerializeField] string sceneName;

    private bool secondSceneLoaded;
    private GameObject gameManager;

    public string SceneName { get => sceneName; set => sceneName = value; }



    public void LoadNewScene()
    {
        if (SceneName == "MainMenu")
        {
            gameManager = GameObject.FindGameObjectWithTag("Game Manager");

            Destroy(gameManager.gameObject);
        }
        
    
        SceneManager.LoadScene(SceneName);
  
    }
    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiverHealCard : BaseHealCard
{
    protected override void Awake()
    {
        base.Awake();
        allowed = 3;
        CardID = 8;
    }

    public override void Use(BaseChip chip)
    {
        chip.ChangeState();

    }
}

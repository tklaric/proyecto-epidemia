﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseCard : MonoBehaviour
{
    private int cardID;
    private Vector3 cardOffset;

    private bool isSelected = false;
    private BaseChip chipReference;
    private int cardType; //0 cura - 1 disease - 2 misc.

    private Animator anim;
   
    
    public bool IsSelected { get => isSelected; set => isSelected = value; }
    public BaseChip ChipReference { get => chipReference; set => chipReference = value; }
    public Animator Anim { get => anim; set => anim = value; }
    public int CardType { get => cardType; set => cardType = value; }
    public int CardID { get => cardID; set => cardID = value; }
    public Vector3 CardOffset { get => cardOffset; set => cardOffset = value; }

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    public virtual void ChangeSelectedBool()
    {
        isSelected = !isSelected;
    }

    public void ActivateRendererInSetTime(float time)
    {
        Invoke("ActivateRenderer", time);
    }
    public void ActivateRenderer()
    {
        gameObject.GetComponent<Renderer>().enabled = true;
    }
    public virtual void Use(BaseChip chip)
    {
      
    }
    
    public virtual void UseDefenses(BaseChip chip,Player player)
    {

    }


    public void Discard(Deck deck)
    {

        //saca una carta de la lista de cards in game
        deck.CardsByIdInGame.Remove(CardID);
        //agrega esa carta a cards discarded
        deck.CardsByIdDiscarded.Add(CardID);
        anim.SetBool("IsDiscarded", true);
        Debug.Log("carta descartada");

    }

    private void OnDestroyEvent()
    {
        Destroy(gameObject);
    }

}

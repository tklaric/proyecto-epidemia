﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackBoard : MonoBehaviour
{
    //creo el diccionario con un string y un tipo objeto
    Dictionary<string, object> memories = new Dictionary<string, object>();

    //al ser privado creo los getSets correspondientes para poder acceder

    public object Get(string memoryName)
    {
        //si existe memoryname que me lo devuelva
        if (memories.ContainsKey(memoryName))
        {
            return memories[memoryName];
        }
        else
        {
            return null;
        }
    }

    public void Set(string memoryName, object memoryValue)
    {
        if (memories.ContainsKey(memoryName))
        {
            memories[memoryName] = memoryValue;
        }
        else
        {
            //si no existe lo creo para que lo guarde
            memories.Add(memoryName, memoryValue);
        }
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarManager : MonoBehaviour
{

    [SerializeField] List<bool> availableAvatars = new List<bool>();
    [SerializeField] List<GameObject> players = new List<GameObject>();

    public List<bool> AvailableAvatars { get => availableAvatars; set => availableAvatars = value; }

    void Start()
    {
        availableAvatars[0] = false;
        
    }

   

}

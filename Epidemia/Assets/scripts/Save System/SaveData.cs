﻿using System.Collections.Generic;
[System.Serializable]
public class SaveData
{
    public List<string> SavetranslateProText = new List<string>();
    public List<string> SavetranslatePauseText = new List<string>();



    public SaveData()
    {

    }

    public SaveData(List<string> SavetranslateProText, List<string> SavetranslatePauseText)
    {
        this.SavetranslateProText = SavetranslateProText;
        this.SavetranslatePauseText = SavetranslatePauseText;
    }

    public override string ToString()
    {
        string output = "";
        //output += "myInt = " + entero + "\n";


        output += "myListPro = ";
        foreach (string i in SavetranslateProText)
            output += i + ", ";
        output += "myListPause = ";
        foreach (string i in SavetranslatePauseText)
            output += i + ", ";

        return output;
    }
}

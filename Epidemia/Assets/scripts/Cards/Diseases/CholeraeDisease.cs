﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CholeraeDisease : BaseDisease
{
    //afecta lungs y kidneys
    protected override void Awake()
    {
        base.Awake();
        allowed1 = 2; //lungs
        allowed2 = 4; //kidneys
        CardID = 3;
    }

    private void Update()
    {
       
        if (IsSelected == true)
        {
            Debug.Log("cholera seleccionada");
        }
    }

    public override void Use(BaseChip chip)
    {
        Debug.Log("cholera usado");

        chip.ChangeState();

        if (chip.Player.HasLowDefenses == true)
        {
            if (chip.Player.PlayerChips[allowed1].IsHealthyChip())
            {
                chip.Player.PlayerChips[allowed1].ChangeState();
            }

            if (chip.Player.PlayerChips[allowed2].IsHealthyChip())
            {
                chip.Player.PlayerChips[allowed2].ChangeState();
            }
        }
    }
   
}

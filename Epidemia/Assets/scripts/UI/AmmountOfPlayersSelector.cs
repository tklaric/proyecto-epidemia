﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AmmountOfPlayersSelector : MonoBehaviour
{

    [SerializeField] TextMeshProUGUI ammountOfPlayersText;
    [SerializeField] List<GameObject> playersUIGO = new List<GameObject>();

    private int ammountOfPlayers = 4;
    private int maxAmmount = 6;
    private int minAmmount = 4;

   

    public int AmmountOfPlayers { get => ammountOfPlayers; set => ammountOfPlayers = value; }

    void Update()
    {
        ammountOfPlayersText.text = AmmountOfPlayers.ToString();

       
        if (AmmountOfPlayers == 4)
        {
            playersUIGO[0].SetActive(true);
            playersUIGO[1].SetActive(true);
            playersUIGO[2].SetActive(true);
            playersUIGO[3].SetActive(true);
            playersUIGO[4].SetActive(false);
            playersUIGO[5].SetActive(false);

           
        }
        else  if (AmmountOfPlayers == 5)
        {
            playersUIGO[4].SetActive(true);
            playersUIGO[5].SetActive(false);
          

        }
        else if (AmmountOfPlayers == 6)
        {
            playersUIGO[5].SetActive(true);

        }

    }
    public void IncreaseAmmount()
    {
        AmmountOfPlayers++;

        if (AmmountOfPlayers >= maxAmmount)
        {
            AmmountOfPlayers = maxAmmount;
        }
    }

    public void DecreaseAmmount()
    {
        AmmountOfPlayers--;

        if (AmmountOfPlayers <= minAmmount)
        {
            AmmountOfPlayers = minAmmount;
        }
    }
}

﻿Shader "Unlit/Simple Gradient"
{
	Properties
	{
		_Color01("Color 01", Color) = (1,1,1,1)
		_Color02("Color 02", Color) = (0,0,0,0)
	}
		SubShader
	{
		Tags { "RenderType" = "Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			float4 _Color01;
			float4 _Color02;

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				float blend = i.uv.y;//VALOR POR EL CUAL SE HACE EL LERP
				fixed4 col = lerp(_Color01, _Color02, blend);//TRANCICIONA DE UN COLOR A OTRO
				return col;
			}
			ENDCG
		}
	}
}

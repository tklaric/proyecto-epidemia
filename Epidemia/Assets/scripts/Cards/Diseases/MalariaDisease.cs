﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MalariaDisease : BaseDisease
{
    //afecta lungs y liver
    protected override void Awake()
    {
        base.Awake();
        allowed1 = 2; //lungs
        allowed2 = 3; //liver
        CardID = 2;
    }

    private void Update()
    {

        if (IsSelected == true)
        {
            Debug.Log("Malaria seleccionada");
            // funcionalidad para poder elegir cual de los 2 chips atacar.
            // if(ficha seleccionada corresponde a alguna de las 2){ use()}

        }
    }
    public override void Use(BaseChip chip)
    {
        Debug.Log("Malaria usado");

        chip.ChangeState();

        if (chip.Player.HasLowDefenses == true)
        {
            if (chip.Player.PlayerChips[allowed1].IsHealthyChip())
            {
                chip.Player.PlayerChips[allowed1].ChangeState();
            }

            if (chip.Player.PlayerChips[allowed2].IsHealthyChip())
            {
                chip.Player.PlayerChips[allowed2].ChangeState();
            }
        }
    }
  
}

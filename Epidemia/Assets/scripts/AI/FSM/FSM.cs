﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSM : MonoBehaviour
{
     //AIController aiController;
     AIController aIController;
     [SerializeField] List<State> states;
     [SerializeField] State currenState;

     void Awake()
     {
        //aiController = GetComponent<AIController>();
        aIController = GetComponent<AIController>();
        if(currenState == null)
        {
            return;
        }

        currenState.InitState(aIController);
     }

     void Update()
     {
        currenState.UpdateState(aIController);    
     }
    
    public void SetState(string statename)
    {
        for (int i = 0; i < states.Count; i++)
        {
            if(states[i].stateName == statename)
            {
                if(currenState != null)
                {
                    currenState.EndState(aIController);
                }

                currenState = states[i];
                currenState.InitState(aIController);

                break;
            }
        }
    }

}

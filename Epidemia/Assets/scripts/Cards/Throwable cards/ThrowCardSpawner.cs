﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowCardSpawner : MonoBehaviour
{
    ObjectPooler objectPooler;


    private void Start()
    {
        objectPooler = ObjectPooler.Instance;
    }

    public void SpawnCard()
    {
        objectPooler.SpawnFromPool("ThrowCard", transform.position, Quaternion.Euler(90,0,0));
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrainHealCard : BaseHealCard
{
    protected override void Awake()
    {
        base.Awake();
        allowed = 0;
        CardID = 6;
    }

    public override void Use(BaseChip chip)
    {
        Debug.Log("Brain Heal Card Used");
        chip.ChangeState();
    }
}

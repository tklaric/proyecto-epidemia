﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KidneysHealCard : BaseHealCard
{
    protected override void Awake()
    {
        base.Awake();
        allowed = 4;
        CardID = 9;
    }

    public override void Use(BaseChip chip)
    {
        chip.ChangeState();
    }
}

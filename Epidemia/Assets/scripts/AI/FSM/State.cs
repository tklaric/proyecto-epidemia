﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State : MonoBehaviour
{
    public string stateName;

    public virtual void InitState(AIController ai)
    {
    }

    public virtual void UpdateState(AIController ai)
    {
    }

    public virtual void EndState(AIController ai)
    {
    }
}

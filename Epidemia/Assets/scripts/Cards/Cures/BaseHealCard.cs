﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseHealCard : BaseCard
{
   // protected BaseChip chipReference;

    //0 brain. 1 heart. 2 lungs. 3 liver. 4 kidneys
    public int allowed;

    protected virtual void Awake()
    {
        CardType = 0;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deck : MonoBehaviour
{
    //ammount of cards in deck
    [SerializeField] int tetanusCardAmmount;
    [SerializeField] int rubellaCardAmmount;
    [SerializeField] int malariaCardAmmount;
    [SerializeField] int choleraeCardAmmount;
    [SerializeField] int smallpoxCardAmmount;
    [SerializeField] int transplantCardAmmount;
    [SerializeField] int brainHealCardAmmount;
    [SerializeField] int heartHealCardAmmount;
    [SerializeField] int liverHealCardAmmount;
    [SerializeField] int kidneysHealCardAmmount;
    [SerializeField] int lungsHealCardAmmount;
    [SerializeField] int lowDefensesCardAmmount;
    [SerializeField] int highDefensesCardAmmount;
    [SerializeField] int EpidemyCardAmmount;

    //to use when instantiating
    [SerializeField] GameObject tetanusCard;
    [SerializeField] GameObject rubellaCard;
    [SerializeField] GameObject malariaCard;
    [SerializeField] GameObject choleraeCard;
    [SerializeField] GameObject smallpoxCard;
    [SerializeField] GameObject transplantCard;
    [SerializeField] GameObject brainHealCard;
    [SerializeField] GameObject heartHealCard;
    [SerializeField] GameObject liverHealCard;
    [SerializeField] GameObject kidneysHealCard;
    [SerializeField] GameObject lungsHealCard;
    [SerializeField] GameObject lowDefenseCard;
    [SerializeField] GameObject highDefensesCard;
    [SerializeField] GameObject epidemyCard;



    private List<GameObject> cardTypesList = new List<GameObject>();
    private List<int> cardsByIdInDeck = new List<int>();
    private List<int> cardsByIdInGame = new List<int>();
    private List<int> cardsByIdDiscarded = new List<int>();

    public List<int> CardsByIdInGame { get => cardsByIdInGame; set => cardsByIdInGame = value; }
    public List<int> CardsByIdDiscarded { get => cardsByIdDiscarded; set => cardsByIdDiscarded = value; }
    public List<int> CardsByIdInDeck { get => cardsByIdInDeck; set => cardsByIdInDeck = value; }
    public List<GameObject> CardTypesList { get => cardTypesList; set => cardTypesList = value; }
    public int BrainHealCardAmmount { get => brainHealCardAmmount; set => brainHealCardAmmount = value; }
    public int HeartHealCardAmmount { get => heartHealCardAmmount; set => heartHealCardAmmount = value; }
    public int LiverHealCardAmmount { get => liverHealCardAmmount; set => liverHealCardAmmount = value; }
    public int KidneysHealCardAmmount { get => kidneysHealCardAmmount; set => kidneysHealCardAmmount = value; }
    public int LungsHealCardAmmount { get => lungsHealCardAmmount; set => lungsHealCardAmmount = value; }
    public GameObject BrainHealCard { get => brainHealCard; set => brainHealCard = value; }

    //cuando una carta se toma del mazo se agrega esa carta a cardsByIdInGame.
    //cuando una carta se juega se pone en cardsByIdInDeck
    //pedir cualquier carta implica una funcion 

    private void Start()
    {
        BuildCardTypeList();
        BuildDeck();
        Shuffle();
     // en caso de de que sea IA puede aparecer una UI de esta jgando la IA espera
    }

    public void Shuffle()
    {
        int cardsInDeck = CardsByIdInDeck.Count;
        List<int> auxList = new List<int>();
        for (int i = 0; i < cardsInDeck; i++)
        {
            int randomNumber = Random.Range(0, CardsByIdInDeck.Count); 
            auxList.Add(CardsByIdInDeck[randomNumber]);
            CardsByIdInDeck.RemoveAt(randomNumber);
        }
        CardsByIdInDeck = auxList;
    }

    private void BuildCardTypeList() 
    {     
        CardTypesList.Add(tetanusCard);
        CardTypesList.Add(rubellaCard);
        CardTypesList.Add(malariaCard);
        CardTypesList.Add(choleraeCard);
        CardTypesList.Add(smallpoxCard);
        CardTypesList.Add(transplantCard);
        CardTypesList.Add(BrainHealCard);
        CardTypesList.Add(heartHealCard);
        CardTypesList.Add(liverHealCard);
        CardTypesList.Add(kidneysHealCard);
        CardTypesList.Add(lungsHealCard);
        CardTypesList.Add(lowDefenseCard);
        CardTypesList.Add(highDefensesCard);
        CardTypesList.Add(epidemyCard);

    }

    private void BuildDeck() 
    {
        for (int i = 0; i < tetanusCardAmmount; i++)
        {
            CardsByIdInDeck.Add(0);
        }
        for (int i = 0; i < rubellaCardAmmount; i++)
        {
            CardsByIdInDeck.Add(1);
        }
        for (int i = 0; i < malariaCardAmmount; i++)
        {
            CardsByIdInDeck.Add(2);
        }
        for (int i = 0; i < choleraeCardAmmount; i++)
        {
            CardsByIdInDeck.Add(3);
        }
        for (int i = 0; i < smallpoxCardAmmount; i++)
        {
            CardsByIdInDeck.Add(4);
        }
        for (int i = 0; i < transplantCardAmmount; i++)
        {
            CardsByIdInDeck.Add(5);
        }
        for (int i = 0; i < BrainHealCardAmmount; i++)
        {
            CardsByIdInDeck.Add(6);
        }
        for (int i = 0; i < HeartHealCardAmmount; i++)
        {
            CardsByIdInDeck.Add(7);
        }
        for (int i = 0; i < LiverHealCardAmmount; i++)
        {
            CardsByIdInDeck.Add(8);
        }
        for (int i = 0; i < KidneysHealCardAmmount; i++)
        {
            CardsByIdInDeck.Add(9);
        }
        for (int i = 0; i < LungsHealCardAmmount; i++)
        {
            CardsByIdInDeck.Add(10);
        }
        for (int i = 0; i < lowDefensesCardAmmount; i++)
        {
            CardsByIdInDeck.Add(11);
        }
        for (int i = 0; i < highDefensesCardAmmount; i++)
        {
            CardsByIdInDeck.Add(12);
        }
        for (int i = 0; i < EpidemyCardAmmount; i++)
        {
            CardsByIdInDeck.Add(13);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseDisease : BaseCard
{
    //podria tenere una referencia al bool de haslowdefenses para ver si ataca uno o ambos organos
    // las cartas enfermedad tendrian otra funcion de use que se active cuando el bool es true y ataquen ambos organos?
    protected BrainChip brainChip;
    protected HeartChip heartChip;
    protected LiverChip liverChip;
    protected LungsChip lungsChip;
    protected KidneysChip kidneysChip;

    //0 brain . 1 heart . 2 lungs . 3 liver . 4 kidneys
    public int allowed1;
    public int allowed2;

    
    protected virtual void Awake()
    {
        CardType = 1;
    }
}

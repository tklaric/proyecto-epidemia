﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlurEnabler : MonoBehaviour
{
    private Clase06_BlurPostProcess blurPostProcess;

    private void Awake()
    {
        blurPostProcess = GetComponent<Clase06_BlurPostProcess>();
    }
    public void DisableBlur()
    {
        blurPostProcess.enabled = false;
    }
    public void EnableBlur()
    {
        blurPostProcess.enabled = true;
    }
}

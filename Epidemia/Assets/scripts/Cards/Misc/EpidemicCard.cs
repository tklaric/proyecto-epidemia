﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EpidemicCard : BaseMisc
{
    protected override void Awake()
    {
        base.Awake();
        MiscType = 3;
        CardID = 13;
    }

    public override void Use(BaseChip chip)
    {
        
    }
}

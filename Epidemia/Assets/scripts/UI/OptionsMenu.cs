﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using TMPro;

public class OptionsMenu : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI resolutionText;

    [SerializeField] TextMeshProUGUI musicVolumeText;

    [SerializeField] TextMeshProUGUI gameSoundsVolumeText;

    [SerializeField] TextMeshProUGUI languageText;

    [SerializeField] AudioMixer musicVolumeMixer;

    [SerializeField] AudioMixer gameSoundsMixer;

    [SerializeField] Resolution[] resolutions;

    [SerializeField] List<string> resolutionOptions = new List<string>();

    private int currentResolutionIndex;
    private int languageIndex;


    private float musicVolume = 100;
    private float gameSoundsVolume = 100;

    void Start()
    {
        resolutions = Screen.resolutions;

        languageText.text = "ENGLISH";

        musicVolumeMixer.SetFloat("MusicVolume", musicVolume-80); // seteo el volumen al mango
        gameSoundsMixer.SetFloat("GameSoundsVolume", gameSoundsVolume-80); // seteo el volumen al mango


        musicVolumeText.text = musicVolume.ToString(); // hago de cuenta que 20db es igual a 100 en texto
        gameSoundsVolumeText.text = gameSoundsVolume.ToString();

        for (int i = 0; i < resolutions.Length; i++)
        {
            resolutionText.text = resolutions[i].width + "x" + resolutions[i].height;

            resolutionOptions.Add(resolutionText.text);

            if (resolutions[i].width == Screen.currentResolution.width&& resolutions[i].height == Screen.currentResolution.height)
            {
                currentResolutionIndex = i;
                resolutionText.text = Screen.currentResolution.width + "x" + Screen.currentResolution.height;
            }

          
        }


    }

    public void IncreaseResolution()
    {
        currentResolutionIndex++;

        if (currentResolutionIndex > resolutionOptions.Count-1)
        {
            currentResolutionIndex = resolutionOptions.Count-1;
        }

        resolutionText.text = resolutionOptions[currentResolutionIndex];

        Resolution resolution = resolutions[currentResolutionIndex];

        Screen.SetResolution(resolution.width, resolution.height,Screen.fullScreen);
        Debug.LogError(currentResolutionIndex);
        Debug.LogError(resolutions[currentResolutionIndex]);
    }

    public void DecreaseResolution()
    {
        currentResolutionIndex--;    

        if (currentResolutionIndex < 0)
        {
            currentResolutionIndex = 0;
        }

        resolutionText.text = resolutionOptions[currentResolutionIndex];

        Resolution resolution = resolutions[currentResolutionIndex];

        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);

    }

    public void IncreaseVolume()
    {
         musicVolume += 10;

        if (musicVolume >= 100) 
        {
            musicVolume = 100;
        }

        musicVolumeMixer.SetFloat("MusicVolume", musicVolume - 80);

        musicVolumeText.text = musicVolume.ToString();
    }

    public void DeacreaseVolume()
    {
        musicVolume -= 10;

        if (musicVolume<=0)
        {
            musicVolume = 0;
        }
        musicVolumeMixer.SetFloat("MusicVolume", musicVolume - 80);

        musicVolumeText.text = musicVolume.ToString();
    }

    public void IncreaseGameSoundsVolume()
    {
        gameSoundsVolume += 10;

        if (gameSoundsVolume >= 100)
        {
            gameSoundsVolume = 100;
        }

        gameSoundsMixer.SetFloat("GameSoundsVolume", gameSoundsVolume - 80);

        gameSoundsVolumeText.text = gameSoundsVolume.ToString();
    }

    public void DecreaseGameSoundsVolume()
    {
        gameSoundsVolume -= 10;

        if (gameSoundsVolume <= 0)
        {
            gameSoundsVolume = 0;
        }

        gameSoundsMixer.SetFloat("GameSoundsVolume", gameSoundsVolume - 80);

        gameSoundsVolumeText.text = gameSoundsVolume.ToString();
    }

    public void IncreaseLanguageIndex()
    {
        languageIndex++;

        languageText.text = "SPANISH";

        if (languageIndex>=1)
        {
            languageIndex = 1;
        }
    }

    public void DecreaseLanguageIndex()
    {
        languageIndex--;

        languageText.text = "ENGLISH";

        if (languageIndex <= 0)
        {
            languageIndex = 0;
        }
    }
}

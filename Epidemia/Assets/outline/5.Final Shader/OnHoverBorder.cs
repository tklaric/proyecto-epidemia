﻿using UnityEngine;

public class OnHoverBorder : MonoBehaviour
{
	public Material border;
	public Material nonBorder;
    private Vector3 original_scale;
    [SerializeField] float scale_multiplier;
    private Vector3 bigger_scale;
    private Renderer renderer;
    [SerializeField] float yValue;

    private void Awake()
    {
        original_scale = gameObject.transform.localScale;
        bigger_scale = original_scale * scale_multiplier;
        renderer = GetComponent<Renderer>();
        renderer.material = nonBorder;
    }
    void OnMouseEnter()
    {
        transform.localPosition += new Vector3(0, yValue, 0);
        //renderer.material = border;
        gameObject.transform.localScale = bigger_scale;
    }

    void OnMouseExit()
    {
        transform.localPosition += new Vector3(0, -yValue, 0);
        //renderer.material = nonBorder;
        gameObject.transform.localScale = original_scale;
    }
}

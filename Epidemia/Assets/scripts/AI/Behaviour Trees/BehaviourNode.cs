﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BehaviourResult { None, Success, Failure, Processing }

public abstract class BehaviourNode : MonoBehaviour
{
    BehaviourResult lastResult = BehaviourResult.None;

    public BehaviourResult LastResult => lastResult;

    public virtual void Init(AIController ai) { }
    public virtual void End(AIController ai) { }

    public virtual bool CanExecute(AIController ai) => true;


    public BehaviourResult Execute(AIController ai)
    {
        if (lastResult != BehaviourResult.Processing)
            Init(ai);


        var newResult = ExecuteInternal(ai);
        lastResult = newResult;

        if (newResult != BehaviourResult.Processing)
            End(ai);



        return newResult;
    }
    protected abstract BehaviourResult ExecuteInternal(AIController ai);
}

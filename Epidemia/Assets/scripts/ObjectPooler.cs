﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    [SerializeField] GameObject cardSpawnerReference;
    [SerializeField] Vector3 allMightyFixingVector;

    [System.Serializable]
    public class Pool
    {
        public string tag;
        public GameObject prefab;
        public int size;
    }

    public List<Pool> pools;
    public Dictionary<string, Queue<GameObject>> pooledDictionary;

    public static ObjectPooler Instance; //singleton
    private void Awake()
    {
        Instance = this;
        
    }

    void Start()
    {
        pooledDictionary = new Dictionary<string, Queue<GameObject>>();

        foreach (Pool pool in pools)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();

            for (int i = 0; i < pool.size; i++)
            {
                GameObject obj = Instantiate(pool.prefab);
                obj.SetActive(false);
                objectPool.Enqueue(obj);
            }

            pooledDictionary.Add(pool.tag, objectPool);
        }
    }
    public GameObject SpawnFromPool(string tag, Vector3 position, Quaternion rotation)
    {
        if(!pooledDictionary.ContainsKey(tag))
        {
            Debug.LogWarning("No existe el pool con tag: " + tag);
            return null;
        }

        GameObject objectToSpawn = pooledDictionary[tag].Dequeue();

        
        objectToSpawn.transform.position = position;
        objectToSpawn.transform.rotation = cardSpawnerReference.transform.rotation;
        objectToSpawn.transform.Rotate(allMightyFixingVector);
        objectToSpawn.SetActive(true);
        //modificar rigid body

        pooledDictionary[tag].Enqueue(objectToSpawn);

        return objectToSpawn;
    }
    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{
    [SerializeField] FSM fsm;
    [SerializeField] GameObject body;
    [SerializeField] BlackBoard blackBoard;

    public BlackBoard BlackBoard { get => blackBoard; set => blackBoard = value; }
    public FSM Fsm { get => fsm; set => fsm = value; }

    private void Start()
    {
        
    }

    private void Update()
    {
        //aca guardamos cosas en memoria del blackboard
        //blacboard.set("lalala", referencia de algun player.cosa a guardar);
    }
}

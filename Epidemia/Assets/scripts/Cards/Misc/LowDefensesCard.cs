﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LowDefensesCard : BaseMisc
{
    protected override void Awake()
    {
        base.Awake();
        MiscType = 0;
        CardID = 11;
    }
    private void Update()
    {
        if (IsSelected == true)
        {
            Debug.Log("lowdefense seleccionada");
        }
    } 
    
    
    public override void UseDefenses(BaseChip chip,Player player)
    {
        player.HasLowDefenses = true;
        chip.ChangeState();
        Debug.Log("tenes sida ahora");
    }
}

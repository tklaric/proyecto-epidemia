﻿using System.Collections;
using System.Collections.Generic;
using SaveSystem;
using UnityEngine;
using UnityEngine.UI;
using AutoLocalization;
using TMPro;
public class GameManager : MonoBehaviour
{

    [SerializeField] AmmountOfPlayersSelector ammountOfPlayersSelector;
    [SerializeField] LanguageMenu languageMenu;
    [SerializeField] List<PlayerMenu> playersUI = new List<PlayerMenu>();
    [SerializeField] List<TextMeshProUGUI> pauseMenuTextTranslate = new List<TextMeshProUGUI>();
    [SerializeField] Animator fadeAnim;
    [SerializeField] LoadScene sceneLoader;

    public List<string> translatedSaveProText = new List<string>();
    public List<string> translatedSavePauseText = new List<string>();

    private string encrypted;

    private FileSave fileSave = new FileSave(FileFormat.Xml);
    private Cryptography cryptography = new Cryptography("pa$$word");

    private bool isInSecondScene;
    private bool unhealableModeSelected;

    private int ammountOfAIPlayers;
    private int numberOfPlayers = 4; //testear cuando este descomentado toa la wea del board manager

    public int NumberOfPlayers { get => numberOfPlayers; set => numberOfPlayers = value; }
    public bool UnhealableModeSelected { get => unhealableModeSelected; set => unhealableModeSelected = value; }
    public int AmmountOfAIPlayers { get => ammountOfAIPlayers; set => ammountOfAIPlayers = value; }
    public List<PlayerMenu> PlayersUI { get => playersUI; set => playersUI = value; }
    public List<TextMeshProUGUI> PauseMenuTextTranslate { get => pauseMenuTextTranslate; set => pauseMenuTextTranslate = value; }
    public bool IsInSecondScene { get => isInSecondScene; set => isInSecondScene = value; }
    public LanguageMenu LanguageMenu { get => languageMenu; set => languageMenu = value; }

    private void Awake()
    {
        string dataPath = Application.dataPath + "/myArchivo.xml";
        if (dataPath != null)
        {
            Debug.Log("entre");
            //LoadTranslate();
        }
        
        DontDestroyOnLoad(this.gameObject);
    }


    public void TranslatePauseMenu(List<string> pauseMenu)
    {
        if (LanguageMenu.HasBeenTranslated == true)
        {
            for (int i = 0; i < PauseMenuTextTranslate.Count; i++)
            {
                PauseMenuTextTranslate[i].text = LanguageManager.instance.GetMeaning(PauseMenuTextTranslate[i].text, Languages.Spanish).ToUpper();
                pauseMenuTextTranslate[0].text = "CONTINUAR";
                pauseMenuTextTranslate[3].text = "SALIR";
                pauseMenuTextTranslate[6].text = "VOLVER";
                pauseMenuTextTranslate[8].text = "ES TU TURNO";
                pauseMenuTextTranslate[9].text = "COMENZAR";
            }
        }
        else
        {
            for (int i = 0; i < PauseMenuTextTranslate.Count; i++)
            {
                PauseMenuTextTranslate[i].text = pauseMenu[i];
            }
        }
    }

    public void SaveTranslate() // llamo a save desde boardmanager para evitar perder la data del pause menu
    {
        
        
            if (translatedSaveProText.Count == 0)
            {
                for (int i = 0; i < LanguageMenu.textProToTranslate.Count; i++)
                {
                    translatedSaveProText.Add(LanguageMenu.textProToTranslate[i].text); // agrego el text mesh pro para que se guarde
                }
            }

            if (translatedSavePauseText.Count == 0)
            {
                for (int i = 0; i < pauseMenuTextTranslate.Count; i++)
                {
                    translatedSavePauseText.Add(pauseMenuTextTranslate[i].text);
                }
            }

            fileSave.WriteToFile(Application.persistentDataPath + "/myArchivo.xml", new SaveData(translatedSaveProText, translatedSavePauseText));
            fileSave.fileFormat = FileFormat.Binary;
            fileSave.WriteToFile(Application.persistentDataPath + "/myArchivo.bin", new SaveData(translatedSaveProText,translatedSavePauseText));

            SaveData saveDataToEncrypt = new SaveData(translatedSaveProText, translatedSavePauseText);
            encrypted = cryptography.Encrypt(saveDataToEncrypt);
            Debug.Log("Encrypt: " + saveDataToEncrypt + "\nResult: " + encrypted);
          

    }

    public void LoadTranslate()
    {
        fileSave.fileFormat = FileFormat.Xml;
        SaveData savedLanguage = fileSave.ReadFromFile<SaveData>(Application.persistentDataPath + "/myArchivo.xml");
        Debug.Log("Loaded data: " + savedLanguage);

        //SaveData saveDataDecrypted = cryptography.Decrypt<SaveData>(encrypted);
        //Debug.Log("Decrypt: " + encrypted + "\nResult: " + saveDataDecrypted);


        for (int i = 0; i < savedLanguage.SavetranslateProText.Count; i++)
        {
            LanguageMenu.textProToTranslate[i].text = savedLanguage.SavetranslateProText[i];
        }

        for (int i = 0; i < savedLanguage.SavetranslatePauseText.Count; i++)
        {
            pauseMenuTextTranslate[i].text = savedLanguage.SavetranslatePauseText[i];
        }
    }


    IEnumerator FadeIn()
    {
        fadeAnim.SetTrigger("FadeIn");
        yield return new WaitForSeconds(1);
        LoadScene();
    }

    public void FadeOut()
    {
        fadeAnim.SetTrigger("FadeOut");
    }

   public void StartFadeIn()
    {
        StartCoroutine(FadeIn());
    }

    public void LoadScene()
    {
        sceneLoader.LoadNewScene();
    }

    public void UpdatePlayerAmmount()
    {
        numberOfPlayers = ammountOfPlayersSelector.AmmountOfPlayers;
    }

    public void UnhealableMode()
    {   
       unhealableModeSelected = true;  
    }

    public void ClassicMode()
    {
        unhealableModeSelected = false;
    }

    public void GameOver( GameObject winningCanvas)
    {
        StopAllCoroutines();
        winningCanvas.SetActive(true);
        
    }



    public void TypeOfPlayerChecker()
    {
        for (int i = 0; i < PlayersUI.Count; i++)
        {
            if (PlayersUI[i].CpuIsActive == true)
            {
                playersUI[i].IsAI = true;
                ammountOfAIPlayers++;
            }
        }
    }
}

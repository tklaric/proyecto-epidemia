﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighDefensesCard : BaseMisc
{
    protected override void Awake()
    {
        base.Awake();
        MiscType = 1;
        CardID = 12;
    }

    public override void UseDefenses(BaseChip chip,Player player)
    {
        player.HasLowDefenses = false;
        chip.ChangeState();
        Debug.Log("ahoar no tenes sida");
    }
}
